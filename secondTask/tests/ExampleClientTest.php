<?php

namespace ExampleClient\Tests;

use ExampleClient\Entity\Comment;
use ExampleClient\ExampleClient;
use ExampleClient\Service\Deserializer;
use ExampleClient\Service\HTTPClient;
use PHPUnit\Framework\TestCase;

class ExampleClientTest extends TestCase
{
    //GET ALL METHOD
    public function getAllSuccessProvider()
    {
        return [
            ['[{"id" : 1, "text": "text", "name": "name"}]', '[{"id" : 1, "text": "text", "name": "name"}]'],
            ['[{"id" : 1, "text": "text", "name": "name"}, {"id" : 2, "text": "text", "name": "name"}]', '[{"id" : 1, "text": "text", "name": "name"}, {"id" : 2, "text": "text", "name": "name"}]'],
            ['[]', '[]'],
        ];
    }

    /**
     * @dataProvider getAllSuccessProvider
     */
    public function testGetAllSuccess($mocked, $expected) : void
    {
        $HTTPClientMock = $this->createMock(HTTPClient::class);
        $HTTPClientMock->method('request')
            ->willReturn($mocked);

        $exampleClient = new ExampleClient($HTTPClientMock);

        $exampleClient->getAll();

        $comments = Deserializer::deserializeArray(json_decode($expected, true));

        $this->assertEquals($comments, $exampleClient->getAll());
    }

    public function getAllErrorProvider()
    {
        $exception = \Exception::class;

        return [
            ['', $exception],
            ['unexpected result or error', $exception],
            ['[unexpected result or error]', $exception],
            [false, $exception],
            [1, $exception],
            [0, $exception],
        ];
    }

    /**
     * @dataProvider getAllErrorProvider
     */
    public function testGetAllError($mocked, $expected) : void
    {
        $this->expectException($expected);

        $HTTPClientMock = $this->createMock(HTTPClient::class);
        $HTTPClientMock->method('request')
            ->willReturn($mocked);

        $exampleClient = new ExampleClient($HTTPClientMock);

        $exampleClient->getAll();
    }

    //CREATE METHOD
    public function createSuccessProvider()
    {
        return [
            ['{"id" : 1, "text": "text", "name": "name"}', '{"text": "text", "name": "name"}', '{"id" : 1, "text": "text", "name": "name"}'],
        ];
    }

    /**
     * @dataProvider createSuccessProvider
     */
    public function testCreateSuccess($mocked, $input, $expected) : void
    {
        $HTTPClientMock = $this->createMock(HTTPClient::class);
        $HTTPClientMock->method('request')
            ->willReturn($mocked);

        $exampleClient = new ExampleClient($HTTPClientMock);

        $expected = Deserializer::deserialize(json_decode($expected, true));
        $input = json_decode($input, true);

        $newComment = new Comment();
        $newComment->setText($input['text']);
        $newComment->setName($input['name']);

        $this->assertEquals($expected, $exampleClient->create($newComment));
    }

    public function createErrorProvider()
    {
        $exception = \Exception::class;
        $input = '{"text": "text", "name": "name"}';

        return [
            ['', $input, $exception],
            ['unexpected result or error', $input, $exception],
            ['[unexpected result or error]', $input, $exception],
            [false, $input, $exception],
            [1, $input, $exception],
            [0, $input, $exception],
        ];
    }

    /**
     * @dataProvider createErrorProvider
     */
    public function testCreateError($mocked, $input, $expected) : void
    {
        $this->expectException($expected);

        $HTTPClientMock = $this->createMock(HTTPClient::class);
        $HTTPClientMock->method('request')
            ->willReturn($mocked);

        $exampleClient = new ExampleClient($HTTPClientMock);

        $input = json_decode($input, true);

        $newComment = new Comment();
        $newComment->setText($input['text']);
        $newComment->setName($input['name']);

        $exampleClient->create($newComment);
    }

    //UPDATE METHOD
    public function updateSuccessProvider()
    {
        $input = '{"id": 1, "text": "text", "name": "name"}';

        return [
            ['{"id" : 1, "text": "text", "name": "name"}', $input, '{"id" : 1, "text": "text", "name": "name"}'],
        ];
    }

    /**
     * @dataProvider updateSuccessProvider
     */
    public function testUpdateSuccess($mocked, $input, $expected) : void
    {
        $HTTPClientMock = $this->createMock(HTTPClient::class);
        $HTTPClientMock->method('request')
            ->willReturn($mocked);

        $exampleClient = new ExampleClient($HTTPClientMock);

        $expected = Deserializer::deserialize(json_decode($expected, true));
        $input = json_decode($input, true);

        $newComment = Deserializer::deserialize($input);

        $this->assertEquals($expected, $exampleClient->update($newComment));
    }

    public function updateErrorProvider()
    {
        $exception = \Exception::class;
        $input = '{"id": 1, "text": "text", "name": "name"}';

        return [
            ['', $input, $exception],
            ['unexpected result or error', $input, $exception],
            ['[unexpected result or error]', $input, $exception],
            [false, $input, $exception],
            [1, $input, $exception],
            [0, $input, $exception],
        ];
    }

    /**
     * @dataProvider updateErrorProvider
     */
    public function testUpdateError($mocked, $input, $expected) : void
    {
        $this->expectException($expected);

        $HTTPClientMock = $this->createMock(HTTPClient::class);
        $HTTPClientMock->method('request')
            ->willReturn($mocked);

        $exampleClient = new ExampleClient($HTTPClientMock);
        $input = json_decode($input, true);

        $newComment = Deserializer::deserialize($input);

        $exampleClient->update($newComment);
    }
}