<?php

namespace ExampleClient;

use ExampleClient\Entity\Comment;
use ExampleClient\Service\Deserializer;
use ExampleClient\Service\HTTPClient;

class ExampleClient
{
    const BASE_URL = "example.com";

    private $httpClient;

    public function __construct(HTTPClient $httpClient = null)
    {
        $this->httpClient = is_null($httpClient) ? new HTTPClient() : $httpClient;
    }

    public function getAll()
    {
        $response = $this->call('GET', self::BASE_URL . '/comments');

        return Deserializer::deserializeArray($response);
    }

    public function create(Comment $comment)
    {
        json_encode($comment);

        $response = $this->call('POST', self::BASE_URL . '/comment', ['comment' => $comment]);

        return Deserializer::deserialize($response);
    }

    public function update(Comment $comment)
    {
        $response = $this->call('PUT', self::BASE_URL . '/comment/' . $comment->getId(), ['comment' => $comment]);

        return Deserializer::deserialize($response);
    }

    protected function call(string $method, string $url, array $options = [])
    {
        $result = $this->httpClient->request($method, $url, $options);

        if ($result === false) {
            throw new \Exception('{"error": 500, "message": "Internal Server Error"}');
        }

        $result = json_decode($result, true);

        if (!is_array($result)) {
            throw new \Exception('{"error": 500, "message": "Internal Server Error"}');
        }

        return $result;
    }
}