<?php


namespace ExampleClient\Service;

use ExampleClient\Entity\Comment;

class Deserializer
{
    public static function deserializeArray(array $comments = null) : array
    {
        $result = [];

        if (is_array($comments)) {
            foreach ($comments as $comment) {
                $result[] = self::deserialize($comment);
            }
        }

        return $result;
    }

    public static function deserialize(array $comment) : Comment
    {
        $result = new Comment();

        $result->setText($comment['text']);
        $result->setId($comment['id']);
        $result->setName($comment['name']);

        return $result;
    }
}