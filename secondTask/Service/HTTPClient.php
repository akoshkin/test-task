<?php


namespace ExampleClient\Service;

class HTTPClient
{
    /**
     * @param $method
     * @param $url
     * @param $options
     * @return bool|string
     */
    public function request($method, $url, $options)
    {
        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

        switch ($method) {
            case 'POST':
                curl_setopt($ch, CURLOPT_POST, 1);
                curl_setopt($ch, CURLOPT_POSTFIELDS, $options);
                break;
            case 'PUT':
                curl_setopt($ch, CURLOPT_PUT, 1);
                curl_setopt($ch, CURLOPT_POSTFIELDS, $options);
                break;
            default:
                //DOING DEFAULT GET REQUEST
                break;
        }

        $output = curl_exec($ch);

        curl_close($ch);

        return $output;
    }
}