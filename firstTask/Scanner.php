<?php

class Scanner
{
    const COUNT_FILE_NAME = 'count';

    public static function scanAndSum(array $folders) : int
    {
        $result = 0;

        foreach ($folders as $folder) {
            try {
                $objects = new RecursiveIteratorIterator(new RecursiveDirectoryIterator($folder['path']), RecursiveIteratorIterator::SELF_FIRST);
                foreach ($objects as $object) {
                    if ($object->getFilename() === self::COUNT_FILE_NAME) {
                        $count = (int)file_get_contents($object->getPathname());

                        $result += $count;
                    }
                }
            } catch (UnexpectedValueException $e) {
                self::logError($e->getMessage());
            }
        }

        return $result;
    }

    private static function logError($message)
    {
        print_r($message . "\n");
    }
}