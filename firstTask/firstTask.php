<?php

//$ php firstTask.php '{"paths": [{"path": "folder"}, {"path": "folder2"}]}'

include_once "Scanner.php";

$folders = json_decode($argv[1], true);

$scanner = new Scanner();

$result = $scanner->scanAndSum($folders['paths']);

print_r($result . "\n");
